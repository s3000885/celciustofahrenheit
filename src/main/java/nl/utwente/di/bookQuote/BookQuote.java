package nl.utwente.di.bookQuote;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Example of a Servlet that gets a temperature in celsius and converts it to fahrenheit
 */

public class BookQuote extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Quoter quoter;
    private Calculator calculator;

    public void init() throws ServletException {
        quoter = new Quoter();
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Celsius to Fahrenheit";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>That is " +
                calculator.getFahrenheit(request.getParameter("celsius")) +
                " Fahrenheit\n</P>" +
                "<a href=\"http://localhost:8080/bookQuote/\" style=\"text-transform:uppercase\">return</a>"+
                "</BODY></HTML>");
    }
}
