package nl.utwente.di.bookQuote;

import java.util.Objects;

public class Calculator {

    public double getFahrenheit(String celsius) {
        if (Objects.equals(celsius, "")) {
            return 0;
        } else {
            try {
                return Integer.parseInt(celsius) * 1.8 + 32;
            } catch (NumberFormatException e) {
                return 0;
            }
        }

    }

}
